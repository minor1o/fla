#include "api.hpp"
#include <unordered_map>
#include <map>
#include <queue>
#include <unordered_set>
#include <algorithm>
#define dstate "dead"

void fillP(const DFA &d, std::vector<std::unordered_set<std::string>> &P) { // P<-{F, Q\F}
  auto final_states = d.get_final_states();
  P.emplace_back(final_states.begin(), final_states.end()); //P[0]<-F
  P.emplace_back();
  for (const auto &q : d.get_states()) {
    if (P[0].contains(q)) continue;
    P.back().insert(q);
  }
}

void fillClass(const std::vector<std::unordered_set<std::string>> &P,
               std::unordered_map<std::string, size_t> &Class) {
  for (int i = 0; i < P.size(); ++i)
    for (const auto &r : P[i]) Class[r] = i;
}

void fillInv(const DFA &d, std::map<std::pair<std::string, char>, std::unordered_set<std::string>> &Inv) {
  for (const auto &q : d.get_states())
    for (const auto &c : d.get_alphabet())
      if (d.has_trans(q, c))
        Inv[{d.get_trans(q, c), c}].insert(q);
}

void deleteUnreachable(DFA &d) {
  std::unordered_set<std::string> reachable;
  std::queue<std::string> queue;
  queue.emplace(d.get_initial_state());
  reachable.emplace(d.get_initial_state());
  while (!queue.empty()) {
    auto from = queue.front();
    queue.pop();
    for (const auto &c : d.get_alphabet()) {
      if (!d.has_trans(from, c)) continue;

      auto next = d.get_trans(from, c);
      if (!reachable.contains(next)) {
        reachable.emplace(next);
        queue.push(next);
      }
    }
  }

  for (const auto &q : d.get_states())
    if (!reachable.contains(q)) d.delete_state(q);
}

void initDead(DFA &d) {
  d.create_state(dstate);
  for (const auto &c : d.get_alphabet())
    d.set_trans(dstate, c, dstate);

  for (const auto &q : d.get_states())
    for (const auto &c : d.get_alphabet())
      if (!d.has_trans(q, c))
        d.set_trans(q, c, dstate);
}

std::pair<std::vector<std::unordered_set<std::string>>, std::unordered_map<std::string, size_t>>
findEquivalenceClasses(DFA &d) { //alternative realisation of Hopcroft algorithm from neerc.ifmo
  std::vector<std::unordered_set<std::string>> P;
  fillP(d, P); // all equivalence classes
  std::unordered_map<std::string, size_t> Class;
  fillClass(P, Class); // Class[r] - index of class P with state r
  std::map<std::pair<std::string, char>, std::unordered_set<std::string>> Inv;
  fillInv(d, Inv); // Inv{{r, a}, set} maps pair {r, a} to set of states which have transitions by symbol 'a' to state r

  std::queue<std::pair<std::unordered_set<std::string>, char>> Queue;
  // queue of pairs {C, a} - equivalence class of states C and symbol from alphabet 'a'
  for (const auto &c : d.get_alphabet())
    for (const auto &Pk : P)
      Queue.emplace(Pk, c); //push to queue Pk equivalence class

  while (!Queue.empty()) {
    auto [C, a] = Queue.front();
    Queue.pop();
    std::map<int, std::unordered_set<std::string>> Involved; // maps number of equivalence class to vector of states inside this class
    for (const auto &q : C)
      for (const auto &r : Inv[{q, a}])
        Involved[(int) Class[r]].insert(r);

    for (auto const &[i, Involved_i] : Involved) { //iterate through all Involved elements
      if (Involved_i.size() < P[i].size()) { // if |Involved[i]|<|P[i]|
        P.emplace_back(); //create new equivalence class in P

        for (const auto &r : Involved_i) {
          P[i].erase(r);
          P.back().insert(r);
        } // delete all occurrences from previous equivalence class and insert to new

        if (P.back().size() > P[i].size())     // new class should be smaller
          std::swap(P.back(), P[i]);

        for (const auto &r : P.back())
          Class[r] = P.size() - 1;

        for (const auto &c : d.get_alphabet())
          Queue.emplace(P.back(), c);
      }
    }
  }
  return {P, Class};
}

DFA dfa_minim(DFA &d) {
  deleteUnreachable(d);
  initDead(d);

  auto [P, Class] = findEquivalenceClasses(d);
  DFA t = DFA(d.get_alphabet());

  std::unordered_map<size_t, std::string> nq; //new q

  int dead_ind = -1;
  for (int i = 0; i < P.size(); ++i) {
    if (P[i].contains(dstate)) {
      dead_ind = i;
      break;
    }
  }

  for (int i = 0; i < P.size(); ++i) {
    if (i == dead_ind) continue;
    nq[i] = "S_" + std::to_string(i) + "_S";
    t.create_state(nq[i]);
    if (std::any_of(P[i].begin(), P[i].end(), [&](auto q) { return d.is_initial(q); }))
      t.set_initial(nq[i]);

    if (std::any_of(P[i].begin(), P[i].end(), [&](auto q) { return d.is_final(q); }))
      t.make_final(nq[i]);
  }

  for (const auto &q : d.get_states())
    for (const auto &c : d.get_alphabet())
      if (d.has_trans(q, c) && !P[dead_ind].contains(d.get_trans(q, c)))
        t.set_trans(nq[Class[q]], c, nq[Class[d.get_trans(q, c)]]);

  deleteUnreachable(t);
  return t;
}