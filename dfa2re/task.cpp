#include "api.hpp"
#include <string>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <utility>

#define f_state "final state"
#define s_state "start state"
#define epsilon '#'

class NGFA {
 public:
  std::map<std::pair<std::string, std::string>, std::unordered_set<std::string>> re_trans;
  std::unordered_map<std::string, std::unordered_set<std::string>> forward_trans, backward_trans;
  DFA d;

  explicit NGFA(DFA a) : d(std::move(a)) {
    create_fiction_nodes();
    init_re_trans();
  }
  //create fiction starting state and connect to old initial state by eps
  void create_fiction_nodes() { //create fiction final state, connect old final states to it
    auto alphabet = d.get_alphabet();
    alphabet.insert(epsilon);
    DFA t(alphabet);
    /* create fictional start state*/
    t.create_state(s_state, true);
    t.create_state(d.get_initial_state());
    t.set_trans(s_state, epsilon, d.get_initial_state());
    /* create fictional final node, link old final states to new*/
    t.create_state(f_state);
    t.make_final(f_state);
    for (const auto &from : d.get_states()) {
      if (!t.has_state(from)) t.create_state(from);
      for (const auto &by : d.get_alphabet())
        if (d.has_trans(from, by)) { //copy old transitions
          auto to = d.get_trans(from, by);
          t.create_state(to);
          t.set_trans(from, by, to);
        }

      if (d.is_final(from)) //if state was final, link to new fictional final state
        t.set_trans(from, epsilon, f_state);
    }
    d = t;
  }
  //initialize re transitions [from] -{a|...|z}-> [to]
  void init_re_trans() {
    for (const auto &from : d.get_states())
      for (const auto &by : d.get_alphabet())
        if (d.has_trans(from, by)) {
          auto to = d.get_trans(from, by);
          re_trans[{from, to}].emplace(1, by);
          forward_trans[from].emplace(to);
          backward_trans[to].emplace(from);
        }
  }

  //merge them in one RE (a|...|z) if transition w. such symbol exists
  std::string trans_str(const std::unordered_set<std::string> &st) {
    std::string trans_str = "(" + (*st.begin());
    for (auto it = std::next(st.begin()); it != st.end(); ++it) trans_str += ("|" + (*it));
    return (trans_str + ")");
  }

  [[nodiscard]] std::string to_re() {
    for (const auto &mid : d.get_states()) {
      if (mid == s_state || mid == f_state) continue;
      for (const auto &to : forward_trans[mid])
        for (auto &from : backward_trans[mid]) {
          std::string tmp = trans_str(re_trans[{from, mid}]) +
              (((re_trans.contains({mid, mid})) && (!re_trans.at({mid, mid}).empty())) ? "(" + trans_str(re_trans.at({mid, mid})) + ")*" : "") +
              trans_str(re_trans[{mid, to}]);
          if (!tmp.empty()) {
            backward_trans[to].emplace(from);
            forward_trans[from].emplace(to);
            re_trans[{from, to}].emplace(tmp);
          }
        }
    }
    std::string re = trans_str(re_trans.at({s_state, f_state}));
    size_t i;
    while ((i = re.find(epsilon)) != std::string::npos) {
      re.replace(i, 1, "");
    }
    return re;
  }
};

//implementation of Brzozowski's and McCluskey's algorithm to convert DFA to RE
std::string dfa2re(DFA &d) {
  return NGFA(d).to_re();
}