#include "api.hpp"
#include <utility>
#include <memory>
#include <stack>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#define term '#'
#define eps '_'
inline int precedence(char op) {
  switch (op) {
    case '|': return 1;
    case '.': return 2;
    case '*': return 3;
    case '(':
    case ')': return 0;
    default: return -1;
  }
}
inline bool is_operator(char c) {
  return c == '|' || c == '.' || c == '*';
}
inline bool is_operand(const char c) {
  return !(c == '(' || c == ')' || is_operator(c));
}
inline bool is_binary_operation(const char c) {
  return c == '|' || c == '.';
}

std::string explicify(const std::string &s) { //insert epsilons and explicit operator '.'
  std::string res(1, '(');
  for (int i = 0; i < s.size(); ++i) {
    bool is_prev_operand_or_star = i > 0 && (is_operand(s[i - 1]) || s[i - 1] == '*' || s[i - 1] == ')');
    if (is_prev_operand_or_star && (is_operand(s[i]) || s[i] == '('))
      res += ".";
    if (is_binary_operation(s[i]) && (i == 0 || s[i - 1] == '(' || is_binary_operation(s[i - 1])))
      res += eps;
    res += s[i];
    if (is_binary_operation(s[i]) && (i == s.size() - 1 || s[i + 1] == ')' || is_binary_operation(s[i + 1])))
      res += eps;
  }
  res.insert(res.size() - 2, ")"); // parentess due to problems with parsing and associative operators
  return res;
}
//https://www.geeksforgeeks.org/convert-infix-expression-to-postfix-expression/
std::string postfix(const std::string &s) { //convert infix notation to postfix notation
  std::stack<char> st; //to parse to Syntax Tree
  std::string res;
  for (const auto &c : s) {
    if (precedence(c) == -1) {  // operand
      res += c;
    } else if (c == '(') {
      st.push(c);
    } else if (c == ')') {
      while (!st.empty() && st.top() != '(') {
        res += st.top();
        st.pop();
      }
      st.pop();  // pop the '('
    } else {  // operator
      while (!st.empty() && st.top() != '(' && precedence(c) <= precedence(st.top())) {
        res += st.top();
        st.pop();
      }
      st.push(c);
    }
  }
  while (!st.empty()) { //remainder
    res += st.top();
    st.pop();
  }
  return res;
}

class Tree {
 public:
  struct Node {
    char c;
    bool nullable;
    std::shared_ptr<Node> left, right;
    std::set<int> firstpos, lastpos, followpos;
    Node(char c, int pos = -1) : c(c), left(nullptr), right(nullptr), nullable(false) {
      if (c == '*' || c == eps) nullable = true;
      if (is_operand(c))
        firstpos.emplace(pos), lastpos.emplace(pos);
    }
  };
  int pos;
  std::string re;
  std::shared_ptr<Node> root;
  std::map<int, std::shared_ptr<Node>> mp;
  explicit Tree(std::string s) : re(std::move(s)) {
    pos = 0;
    root = parse();
  }

  std::shared_ptr<Node> parse() {
    std::stack<std::shared_ptr<Node>> st;
    for (const auto &c : re) {
      std::shared_ptr<Node> node;
      if (precedence(c) != -1) {  // Operator
        node = std::make_shared<Node>(c);
        if (c == '*') {  // unary operator
          node->left = st.top();
          st.pop();
        } else {  // binary operator
          node->right = st.top();
          st.pop();
          node->left = st.top();
          st.pop();
        }
      } else {  // Operand
        node = std::make_shared<Node>(c, pos);
        mp[pos++] = node;
      }
      if (node) {
        calculate_node(node);
        st.push(node);
      }
    }
    if (!st.empty()) { //get root and calculate at as other nodes
      calculate_node(st.top());
      return st.top();
    }
    return nullptr;
  }

  void calculate_node(const std::shared_ptr<Node> &n) {
    if (!n) return;
    n->nullable = get_nullable(n); //calculate all needed functions
    n->firstpos = get_firstpos(n);
    n->lastpos = get_lastpos(n);
    get_followpos(n); //calculate followpos after all
  }
  void get_followpos(const std::shared_ptr<Node> &n) {
    if (!n) return;
    get_followpos(n->left);
    get_followpos(n->right);
    if (is_operand(n->c) || n->c == '|') return;
    auto firstpos = ((n->c == '.') ? get_firstpos(n->right) : get_firstpos(n->left));
    for (const auto &i : get_lastpos(n->left))
//      followpos[i].insert(firstpos.begin(), firstpos.end());
      mp[i]->followpos.insert(firstpos.begin(), firstpos.end());
  }
  bool get_nullable(const std::shared_ptr<Node> &n, bool online = false) {
    if (!n) return true; //online mode supposed, that you can get value without recursion with nullptr check
    if (online) return n->nullable;
    get_nullable(n->left);
    get_nullable(n->right);
    if (!is_operator(n->c)) return n->c == eps;
    if (n->c == '*') return true;
    if (n->c == '|') return n->left->nullable || n->right->nullable;
    return n->left->nullable && n->right->nullable;
  }
  std::set<int> get_firstpos(const std::shared_ptr<Node> &n, bool online = false) {
    if (!n) return {};
    if (online) return n->firstpos;
    get_firstpos(n->left);
    get_firstpos(n->right);
    if (!is_operator(n->c)) {
      if (n->c == eps) return {};
      return n->firstpos;
    }
    if (n->c == '*') return n->left->firstpos;
    std::set<int> l = n->left->firstpos, r = n->right->firstpos, lur = {};
    std::set_union(l.begin(), l.end(), r.begin(), r.end(), std::inserter(lur, lur.begin()));
    if (n->c == '|') return lur;
    return (n->left->nullable) ? lur : l;
  }
  std::set<int> get_lastpos(const std::shared_ptr<Node> &n, bool online = false) {
    if (!n) return {};
    if (online) return n->lastpos;
    get_lastpos(n->left);
    get_lastpos(n->right);
    if (!is_operator(n->c)) {
      if (n->c == eps) return {};
      return n->lastpos;
    }
    if (n->c == '*') return n->left->lastpos;
    std::set<int> l = n->left->lastpos, r = n->right->lastpos, lur = {};
    std::set_union(l.begin(), l.end(), r.begin(), r.end(), std::inserter(lur, lur.begin()));
    if (n->c == '|') return lur;
    return (n->right->nullable) ? lur : r;
  }
};

DFA re2dfa(const std::string &s) {
  auto alphabet = Alphabet((s.empty()) ? std::string(1, term) : s);
  DFA d{alphabet};
  if (s.empty()) {
    d.create_state("0", true);
    return d;
  }
  Tree t(postfix(explicify(s + term)));
  std::queue<std::set<int>> Queue;
  std::map<std::set<int>, std::string> states;
  auto q0 = t.root->firstpos;
  states[q0] = "0";
  Queue.emplace(q0);
  d.create_state(states[q0]);
  d.set_initial(states[q0]);
  while (!Queue.empty()) {
    auto R = Queue.front();
    Queue.pop();

    for (const auto &c : alphabet) {
      std::set<int> S;
      for (const auto &pi : R)
        if (c == t.mp[pi]->c)
          S.insert(t.mp[pi]->followpos.begin(), t.mp[pi]->followpos.end());
      if (!S.empty() && !states.contains(S)) {
        auto name = std::to_string(states.size());
        states[S] = name;
        d.create_state(name);
        Queue.push(S);
      }
      if (!S.empty())
        d.set_trans(states[R], c, states[S]);
    }
  }
  for (const auto &[state, name] : states) {
    if (state.contains(t.mp.size() - 1)) {
      d.make_final(name);
    }
  }
  return d;
}